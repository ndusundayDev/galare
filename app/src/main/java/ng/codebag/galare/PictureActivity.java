package ng.codebag.galare;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

/**
 * Created by ndu on 9/8/17.
 */

public class PictureActivity extends AppCompatActivity {
    MainActivity mainActivity;

    ImageView pictureImageView;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture);

        pictureImageView = (ImageView) findViewById(R.id.picuture_image_view);

        Bitmap photo = mainActivity.photo;

        pictureImageView.setImageBitmap(photo);
    }
}
